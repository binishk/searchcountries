var cors = require("cors");
var express = require("express");
var bodyParser = require("body-parser")
const {MongoClient} = require('mongodb')
var createError = require("http-errors");
const password = "4Y7OHto0NQ4G0vds"
const {handleErrors, handleNotFoundError} = require('./middlewares')
const uri = `mongodb+srv://binish:${password}@cluster0.hi6jl.mongodb.net/countries_db?retryWrites=true&w=majority`

// Uniquely identify each user. Only has one user currently with an id of "1"
const USER_ID = "1"
const port = 5000

let collection;

async function connectMongo() {
    try{
        const client = await MongoClient.connect(uri, { useUnifiedTopology: true })
        console.log("connected")
        collection = client.db("countries_db").collection("countries")
        // collection.insertOne({"_id":USER_ID, "selected": []})
    }
    catch(err){
        console.log("error", err)
    }
}

connectMongo();

var app = express();
app.use(cors());

app.use(bodyParser.json())

const getCountries = async (req, res) => {
        try{
            const result = await collection.findOne({"_id": USER_ID})
            res.status(200).json({status: "Done", "data": result.selected})
        }
        catch(err){
            console.log("Error: ", err)
            throw createError(500, "Could not fetch data")
    }
}

const removeCountry = async (req, res) => {
    try{
    const result = await collection.findOneAndUpdate({},
        {$pull: {selected: {name: req.body.name}}},
        { returnOriginal: false },
    )
    res.status(200).json({"message": "Removed a country from the list", "data": result.value.selected})
    }
    catch(err){
        console.log("Error: ", err)
        throw createError(500, "Could not update data")
    }
}

const addCountry = async (req, res) => {
    try{
        const result = await collection.findOneAndUpdate({"_id": USER_ID},
        {
            $push: {"selected": {name: req.body.name, flag: req.body.flag}}
        },
        { upsert: true , returnOriginal: false}
        )
        res.status(200).json({"message": "Added a country to the list", "data": result.value.selected})
    }
    catch(err){
        console.log("Error: ", err)
        throw createError(500, "Could not update data")
    }
}

app.get('/countries', getCountries)
    .delete('/countries', removeCountry)
    .put('/countries', addCountry)  

// catch 404 and forward to error handler
app.use(handleNotFoundError);

// error handler
app.use(handleErrors);

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
  })

module.exports = app;
