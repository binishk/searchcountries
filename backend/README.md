## An server to store the list of selected countries.
All the content has been bundled into a single file app.js. Few parameters 
such as PORT number , User ID, password, etc. have been supplied by default and 
could be implemented dynamically if needed in further iterations.

### Instructions to Run
Clone or download the project and run

```
npm install
```
```
npm start
```
By default runs on 5000 port.

### Libraries Used
- mongodb  
Mongodb Atlas is used as the database to store user data.  
- express  
Express is used to create an API and provide endpoints for various services.  
- body-parser  
Body parser is used to parse the JSON body of the requests.  
- http-errors  
Used to simplify the process of returning error responses  
- cors  
To overcome cors issue if present.  

### Endpoints:

GET http://localhost:5000/countries/
- Retrieves the list of selected countries.

PUT http://localhost:5000/countries/
- Adds a new country to the list of selected countries.

DELETE http://localhost:5000/countries/ 
- Removes a country from the list of selected countries. 

### Errors:

| Error        | Code           | Reason  |
| ------------- |:-------------:| -----:|
| Internal Server | 500 | General errors in the server |
| Not Found       | 404    | Incorrect url pointing to a non-existing resource |

## References

https://restcountries.eu/