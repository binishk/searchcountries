import React from 'react'
import './App.css'

/*
 A context to be shared by both Search and ListItem components.
 ListItem component has access the 'selected' state and also
 to the methods which can make changes to it
*/
const GlobalContext = React.createContext();

const BACKEND_URL = "http://localhost:5000/countries"

class CountryListItem extends React.Component {
  /* 
    Takes an image URL and a country name, and renders a 
    list item that can be pinned to or removed from the
    "Selected Countries" list.
  */

  render() {
    const {country, action_type} = this.props
    return (
        <GlobalContext.Consumer>
        {
          context => (
            <li className="list-group-item">
            <img src={country.flag} alt={`${country.name} flag`} />
            <span className="country-name"> {country.name} </span>
            <button onClick={()=>{
              if (action_type === "add"){
                context.handleSelect(country)
              } else {
                context.handleRemove(country)
              }
            }}> {action_type==="add"?'+':'x'} </button>
          </li>
        )}
        </GlobalContext.Consumer>
    );
  }
}

class Search extends React.Component {
  /* 
    The search bar and search results. Add a handler that makes
    a rest-countries API request on key press, and returns the     
    first 5 results as <CountryListItem /> components. 
    Show a loading state while API request is not resolved!
  */
  constructor(props){
    super(props)
    this.state= {
      countries: [],
      loading: false,
      inputQuery: '',
    }
  }

  // Handles the change in the html input field and updates the related state.
  handleInputQuery = (e) => {
    this.setState({inputQuery: e.target.value, loading: false})
  }

  /*
    An async method which takes in a search query and performs complete or partial search 
    from the API. Updates the searched countries state with top 5 results from the API.
    API results only contain the fields 'name' and 'flag'.
  */
  async searchAPI(query){
    try{
      if (query.length > 0) {
        const response = await fetch(`https://restcountries.eu/rest/v2/name/${query}?fields=name;flag`)
        const result = await response.json()
        this.setState({countries: result.slice(0, 5), loading: false})
      } else {
        this.setState({countries: [], loading: false})
      }
    }
    catch(err){
      throw (err)
    }
  }

  componentDidUpdate(prevProps, prevState) {
    /*
      A function to check for the change in input query. When the input query changes,
      the search API method is called to begin the search/autocomplete feature.
    */
    const update = async() => {
      // Compare states to identify if input query has changed.
      if (this.state.inputQuery !== prevState.inputQuery) {
          try{
            await this.searchAPI(this.state.inputQuery)
          }
          catch(err){
            console.log("Error", err)
            this.setState({countries: [], loading: false})
          }
      }
    }
    update()
  }
  render() {
    return (
      <div className="flex-grow-1 m-1">
      <label for="search" class="visuallyhidden">Search: </label>
        <input
          type="text"
          className="w-100"
          id="search"
          placeholder="Start typing a country name here"
          value={this.state.inputQuery}
          onChange={this.handleInputQuery}
          />
        <h4 className="mt-2">Search Results (part 2): </h4>
        {/* Component has loading state while the API call is in progress. A relevant message is
        displayed at throughout the loading phase. */}
        {this.state.loading ? <h5>Searching ...</h5> : <ul className="list-group pr-2">
          {this.state.countries.map(country=> {
            return <CountryListItem key={country.name} country={country} action_type="add"/>
          })}
        </ul>}
      </div>
    );
  }
}

class SelectedCountries extends React.Component {
  /* 
    A list of selected countries (no duplicates). Replace
    the singular <CountryListItem /> below with the list of 
    all selected countries.
  */
  render() {
    return (
        <div className="flex-grow-1">
          <h4> Selected Countries (part 3): </h4>
          <GlobalContext.Consumer>
            {({selected}) => selected.length>0 
            ? 
            <ul className="list-group pr-2">
                {selected.map(country=> {
                return <CountryListItem key={country.name} country={country} action_type="remove"/>
            })}
            </ul>
            :
            <span className="info-light">No countries selected</span>}
          </GlobalContext.Consumer>
      </div>
    );
  }
}

class ListSearchApp extends React.Component {
  /* 
    The entire app that gets rendered in the "root" 
    element of the page
  */

 constructor(props){
   super(props)

   this.state = {
     selected: []
    }
  }

  /*
    An async method to fetch the selected events from the backend. This method 
    is called each time the ListSearchApp component mounts.
  */
  fetchSelectedCountries = async () => {
    try{
      const response = await fetch(BACKEND_URL)
      const result = await response.json()
      this.setState({selected: result.data})
    }
    catch(err){
      console.log("Error: ", err)
    }
  }
  componentDidMount(){
    // Fetch the user selected data each time the component mounts.
    this.fetchSelectedCountries()
  }
  render() {
    const onSelectCountry = async (newCountry) =>{
      // Avoid selecting the same country twice. Go through the list to
      // see if the country has been selected already
      const isAlreadySelected = this.state.selected.filter(item => newCountry.name === item.name).length>0

      if (!isAlreadySelected){
        /*
         Send a PUT request to the server to add the selected country and update the selected
         country list. Result contains the updated 'selected' data with the new country added in.
         Use this result to update the state locally once the API call is complete.
        */
        try{
          const response = await fetch(BACKEND_URL, {
            method: 'PUT',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify(newCountry)
          })
          const result = await response.json()
          this.setState({selected:result.data})
        }
        catch(err){
          console.log("Error", err)
        }
      }
    }
    const onRemoveCountry = async (country) => {
       /*
         Send a DELETE request to the server to remove the selected country and update the selected
         country list. Result contains the updated 'selected' data with the new country added in.
         Use this result to update the state locally once the API call is complete.
        */
      try{
        const response = await fetch(BACKEND_URL, {
          method: 'DELETE',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(country)
        })
        const result = await response.json()
        // this.setState(({selected}) => ({selected: selected.filter(item => item.name !== country.name)}))
        this.setState({selected: result.data})
      }
      catch(err){
        console.log("Error", err)
      }
    }
    return (
        <div className="row w-100 d-flex pt-2">
        <GlobalContext.Provider value={{selected: this.state.selected, 
                                handleRemove: val => {onRemoveCountry(val)}, 
                                handleSelect: val => {onSelectCountry(val)}}}>
          <Search />
          <SelectedCountries />
        </GlobalContext.Provider>
        </div>
    )
  }
}

export default ListSearchApp