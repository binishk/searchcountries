## A client to search for, list and stored select countries.
React App was written in VS Code. Online Codepen for the app is available
[here](https://codepen.io/foreverdrifting/pen/eYzWpOX).
Requires the backend to be running on localhost at port 5000.

### Libraries Used
- axios  
For HTTP requests.   

## References
https://restcountries.eu/

